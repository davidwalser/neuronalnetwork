﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    public class NeuronalNetwork
    {
        /*private List<InputNeuron> inputNeurons = new List<InputNeuron>();
        private List<HiddenNeuron> hiddenNeurons = new List<HiddenNeuron>();
        private List<OutputNeuron> outputNeurons = new List<OutputNeuron>();
        */
        private InputLayer inputLayer = new InputLayer();
        private HiddenLayer hiddenLayer = new HiddenLayer();
        private OutputLayer outputLayer = new OutputLayer();
        private List<DigitImage> testimages = new List<DigitImage>();
        private List<DigitImage> trainimages = new List<DigitImage>();
        private List<Tuple<int, int, int>> calcdata = new List<Tuple<int, int, int>>();
        private double Accuracy { get; set; }
        private double NetworkError { get; set; } = 1;
        public int[,] Matrix { get; set; } = new int[10,10];

        public NeuronalNetwork(List<Tuple<int, int, int>> data, int inputNeuronsCount = 784, int hiddenNeuronsCount = 89, int outputNeuronCount = 10)
        {
            //todo: bias in parentlayer!!
            calcdata = data;
            inputLayer.ParentLayer = null;
            inputLayer.ChildLayer = hiddenLayer;
            hiddenLayer.ParentLayer = inputLayer;
            hiddenLayer.ChildLayer = outputLayer;
            outputLayer.ParentLayer = hiddenLayer;
            inputLayer.name = "input";
            hiddenLayer.name = "hidden";
            outputLayer.name = "output";
            outputLayer.ChildLayer = null;
            Random random = new Random();
            for (int i = 0; i < outputNeuronCount; i++)
            {
                OutputNeuron neuron = new OutputNeuron();
                //neuron.BiasWeight = random.NextDouble() * 2 - 1;
                outputLayer.Nodes.Add(neuron);
            }
            for (int i = 0; i < hiddenNeuronsCount; i++)
            {
                HiddenNeuron neuron = new HiddenNeuron();
                //neuron.BiasWeight = random.NextDouble() * 2 - 1;
                for (int j = 0; j < outputNeuronCount; j++)
                {
                    Connection connection = new Connection() { Weight = random.NextDouble() * 2 - 1, ComingFromNeuron = neuron, GoingToNeuron = outputLayer.Nodes[j] };
                    outputLayer.Nodes[j].Connections.Add(connection);
                    neuron.Connections.Add(connection);
                   
                }
                hiddenLayer.Nodes.Add(neuron);
            }
            for (int i = 0; i < inputNeuronsCount; i++)
            {
                InputNeuron neuron = new InputNeuron();
                //neuron.BiasWeight = random.NextDouble() * 2 - 1;
                for (int j = 0; j < hiddenNeuronsCount; j++)
                {
                    Connection connection = new Connection() { Weight = random.NextDouble() * 2 - 1, ComingFromNeuron = neuron, GoingToNeuron = hiddenLayer.Nodes[j] };
                    hiddenLayer.Nodes[j].Connections.Add(connection);
                    neuron.Connections.Add(connection);
                    
                }
                inputLayer.Nodes.Add(neuron);
            }
            for (int i = 0; i < outputNeuronCount; i++)
            {
                hiddenLayer.Bias.Add(new Pair<double, double>(1, random.NextDouble() * 2 - 1));
            }
            for (int i = 0; i < hiddenNeuronsCount; i++)
            {
                inputLayer.Bias.Add(new Pair<double, double>(1, random.NextDouble() * 2 - 1));
            }
            inputLayer.Errors = new double[inputNeuronsCount];
            hiddenLayer.Errors = new double[hiddenNeuronsCount];
            outputLayer.Errors = new double[outputNeuronCount];
        }

        public NeuronalNetwork(List<DigitImage> images, List<DigitImage> testimages, int inputNeuronsCount = 784, int hiddenNeuronsCount = 89, int outputNeuronCount = 10)
        {
            this.testimages = images;
            this.trainimages = testimages;
            inputLayer.ParentLayer = null;
            inputLayer.ChildLayer = hiddenLayer;
            hiddenLayer.ParentLayer = inputLayer;
            hiddenLayer.ChildLayer = outputLayer;
            outputLayer.ParentLayer = hiddenLayer;
            inputLayer.name = "input";
            hiddenLayer.name = "hidden";
            outputLayer.name = "output";
            outputLayer.ChildLayer = null;
            Random random = new Random();
            for (int i = 0; i < outputNeuronCount; i++)
            {
                OutputNeuron neuron = new OutputNeuron();
                //neuron.BiasWeight = random.NextDouble() * 2 - 1;
                outputLayer.Nodes.Add(neuron);
            }
            for (int i = 0; i < hiddenNeuronsCount; i++)
            {
                HiddenNeuron neuron = new HiddenNeuron();
                //neuron.BiasWeight = random.NextDouble() * 2 - 1;
                for (int j = 0; j < outputNeuronCount; j++)
                {
                    Connection connection = new Connection() { Weight = random.NextDouble() * 2 - 1, ComingFromNeuron = neuron, GoingToNeuron = outputLayer.Nodes[j] };
                    outputLayer.Nodes[j].Connections.Add(connection);
                    neuron.Connections.Add(connection);
                }
                hiddenLayer.Nodes.Add(neuron);
            }
			for (int i = 0; i < inputNeuronsCount; i++)
            {
				InputNeuron neuron = new InputNeuron();
                //neuron.BiasWeight = random.NextDouble() * 2 - 1;
                for (int j = 0; j < hiddenNeuronsCount; j++)
				{
                    Connection connection = new Connection() { Weight = random.NextDouble() * 2 - 1, ComingFromNeuron = neuron, GoingToNeuron = hiddenLayer.Nodes[j] };
                    hiddenLayer.Nodes[j].Connections.Add(connection);
                    neuron.Connections.Add(connection);
				}
                inputLayer.Nodes.Add(neuron);
            }
            for (int i = 0; i < outputNeuronCount; i++)
            {
                hiddenLayer.Bias.Add(new Pair<double, double>(1, random.NextDouble() * 2 - 1));
            }
            for (int i = 0; i < hiddenNeuronsCount; i++)
            {
                inputLayer.Bias.Add(new Pair<double, double>(1, random.NextDouble() * 2 - 1));
            }
            inputLayer.Errors = new double[inputNeuronsCount];
            hiddenLayer.Errors = new double[hiddenNeuronsCount];
            outputLayer.Errors = new double[outputNeuronCount];
            /*Console.WriteLine("Outputlayer Nodes: " + outputLayer.Nodes.Count);
            for (int i = 0; i < outputLayer.Nodes.Count; i++)
            {
                Console.WriteLine($"OUTConnections: {outputLayer.Nodes[i].Connections.Count}");
            }
            Console.WriteLine("Hiddenlayer Nodes: " + hiddenLayer.Nodes.Count);
            for (int i = 0; i < hiddenLayer.Nodes.Count; i++)
            {
                Console.WriteLine($"HIDConnections: {hiddenLayer.Nodes[i].Connections.Count}");
            }
            Console.WriteLine("Inputlayer Nodes: " + inputLayer.Nodes.Count);
            for (int i = 0; i < inputLayer.Nodes.Count; i++)
            {
                Console.WriteLine($"INConnections: {inputLayer.Nodes[i].Connections.Count}");
            }*/
        }

        public void FeedForward()
        {
            //inputLayer.calculateNeuronValues();
            hiddenLayer.CalculateNeuronValues();
            outputLayer.CalculateNeuronValues();
        }

        public void BackPropagate()
        {
            outputLayer.CalculateErrors();
            hiddenLayer.CalculateErrors();
            hiddenLayer.AdjustWeights();
            inputLayer.AdjustWeights();
        }

        public void TrainData()
        {
            Stopwatch sw = new Stopwatch();
            while (NetworkError >= 0.005)
            {
                sw.Start();
                int[] desired = new int[10];
                for (int i = 0; i < trainimages.Count; i++)
                {
                    for (int j = 0; j < trainimages[i].getData().Length; j++)
                    {
                        inputLayer.Nodes[j].Value = trainimages[i].getData()[j];
                        //Console.WriteLine(j +"," + inputLayer.Nodes[j].Value);
                    }
                    for (int l = 0; l < desired.Length; l++)
                    {
                        desired[l] = 0;
                    }
                    desired[trainimages[i].getLabel()] = 1;
                    outputLayer.DesiredValues = desired;
                    //Console.WriteLine("FeedForward");
                    FeedForward();
                    //Console.WriteLine("BackProgagate");

                    BackPropagate();
                    //Console.WriteLine(i);
                }
                NetworkError = calculateNetworkError();
                Console.WriteLine($"NetworkError: {NetworkError}");
                sw.Stop();
                Console.WriteLine($"{(double)sw.ElapsedMilliseconds / (double)60000} minutes");
                sw.Reset();
            }
        }

        public void TrainCalc()
        {
            while (NetworkError >= 0.005)
            {
                int[] desired = new int[1];
                for (int i = 0; i < calcdata.Count; i++)
                {
                    inputLayer.Nodes[0].Value = (double)calcdata[i].Item1;
                    inputLayer.Nodes[1].Value = (double)calcdata[i].Item2;
                    desired[0] = calcdata[i].Item3;
                    outputLayer.DesiredValues = desired;
                    FeedForward();
                    BackPropagate();
                }
                NetworkError = calculateNetworkError();
                Console.WriteLine($"NetworkError: {NetworkError}");
            }
        }

        public void TestCalc()
        {
            inputLayer.Nodes[0].Value = 0;
            inputLayer.Nodes[1].Value = 1;
            FeedForward();
            Console.WriteLine("TEST: " + outputLayer.Nodes[0].Value);
            inputLayer.Nodes[0].Value = 0;
            inputLayer.Nodes[1].Value = 0;
            FeedForward();
            Console.WriteLine("TEST: " + outputLayer.Nodes[0].Value);
            inputLayer.Nodes[0].Value = 1;
            inputLayer.Nodes[1].Value = 1;
            FeedForward();
            Console.WriteLine("TEST: " + outputLayer.Nodes[0].Value);
            inputLayer.Nodes[0].Value = 1;
            inputLayer.Nodes[1].Value = 0;
            FeedForward();
            Console.WriteLine("TEST: " + outputLayer.Nodes[0].Value);
        }

        public void TestImages()
        {
            int rightSuggest = 0;
            for (int i = 0; i < testimages.Count; i++)
            {
                for (int j = 0; j < testimages[i].getData().Length; j++)
                {
                    inputLayer.Nodes[j].Value = testimages[i].getData()[j];
                    //Console.WriteLine(j +"," + inputLayer.Nodes[j].Value);
                }
                FeedForward();
                //Console.Write("Image was: " + testimages[i].getLabel());
                double diff = 100;
                int suggest = -1;
                for (int o = 0; o < outputLayer.Nodes.Count; o++)
                {
                    double thisdiff = Math.Abs(1 - outputLayer.Nodes[o].Value);
                    if (diff > thisdiff)
                    {
                        suggest = o;
                        diff = thisdiff;
                    }
                }
                //Console.Write("; Suggest was: " + suggest);
                //Console.WriteLine();
                if (suggest == testimages[i].getLabel())
                {
                    rightSuggest++;
                }
                Matrix[suggest, testimages[i].getLabel()]++;
            }
            char[] realvalues = new char[10];
            realvalues = "realvalues".ToCharArray();
            Console.WriteLine($"{"",25} predicted values");
            Console.WriteLine($"{"",5}|{0,5}|{1,5}|{2,5}|{3,5}|{4,5}|{5,5}|{6,5}|{7,5}|{8,5}|{9,5}|");
            for (int i = 0; i < 10; i++)
            {
                Console.Write($"{realvalues[i]} {i,3}|");
                for (int j = 0; j < 10; j++)
                {
                    Console.Write($"{Matrix[i, j],5}|");
                }
                Console.WriteLine("");
            }
            Console.WriteLine($"Accuracy is: {(double)rightSuggest / (double)testimages.Count}");
        }

        public double calculateNetworkError()
        {
            double error = 0;
            for (int i = 0; i < outputLayer.Nodes.Count; i++)
            {
                //Console.WriteLine("outputlayer.nodes.value: " + outputLayer.Nodes[i].Value);
                //Console.WriteLine("Desired: " + outputLayer.DesiredValues[i]);
                error += Math.Pow(outputLayer.Nodes[i].Value - outputLayer.DesiredValues[i], 2);
                //Console.WriteLine("Error: "+error);
            }
            error = error / outputLayer.Nodes.Count;
            Console.WriteLine("NetworkError: " + error);
            return error;
        }
    }
}
