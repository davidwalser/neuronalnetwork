﻿using System;
using System.Linq;
using System.Collections.Generic;
using NeuronalNetwork.Neuron;
using System.IO;
using System.Text.RegularExpressions;

namespace NeuronalNetwork
{
    class Program
    {
        private static List<DigitImage> images = new List<DigitImage>();
        private static List<DigitImage> testimages = new List<DigitImage>();
        private static List<Tuple<int, int, int>> calcdata = new List<Tuple<int, int, int>>();

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            loadImageData();
            Neuron.NeuronalNetwork neuronalNetwork = new Neuron.NeuronalNetwork(images,testimages);
            neuronalNetwork.TrainData();
            neuronalNetwork.TestImages();

            /*loadCalcData();
            Neuron.NeuronalNetwork neuronalNetwork = new Neuron.NeuronalNetwork(calcdata, 2, 3, 1);
            neuronalNetwork.TrainCalc();
            neuronalNetwork.TestCalc();*/
            Console.ReadKey();
        }

        private static void loadImageData()
        {
            DigitImageLoader dil = new DigitImageLoader("t10k-labels-idx1-ubyte.dat", "t10k-images-idx3-ubyte.dat", 10000);
            DigitImageLoader dilTest = new DigitImageLoader("train-labels-idx1-ubyte.dat", "train-images-idx3-ubyte.dat");
            try
            {
                images = dil.LoadData().ToList();
                testimages = dilTest.LoadData().ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        private static void loadCalcData()
        {
            string [] lines = File.ReadAllLines(@"D:\OneDrive - FH Technikum Wien\Technikum\Semester_4\MLE\NeuronalNetwork\NeuronalNetwork\calcdata.txt");
            for (int i = 0; i < lines.Length; i++)
            {
                Console.WriteLine($"{Regex.Split(lines[i], ",")[0]} + {Regex.Split(lines[i], ",")[1]} = {Regex.Split(lines[i], ",")[2]}");
                calcdata.Add(new Tuple<int, int, int>(Convert.ToInt32(Regex.Split(lines[i], ",")[0]), Convert.ToInt32(Regex.Split(lines[i], ",")[1]), Convert.ToInt32(Regex.Split(lines[i], ",")[2])));
            }
        }
    }
}
