﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork
{
    public class DigitImage
    {

        private int label;
        private double[] data;

        public DigitImage(int label, byte[] data)
        {
            this.label = label;

            this.data = new double[data.Length];

            for (int i = 0; i < this.data.Length; i++)
            {
                this.data[i] = data[i] & 0xFF; //convert to unsigned
            }
            otsu();
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(label);
            sb.Append(" = ");
            sb.Append(data.Length);
            sb.Append(": (");
            sb.Append("\n");
            for (int y = 0; y < 28; y++)
            {
                for (int x = 0; x < 28; x++)
                {
                    if ((int)data[(y * 28) + x] == 1)
                    {
                        sb.Append("*");
                    }
                    else
                    {
                        sb.Append(" ");
                    }

                }
                sb.Append("\n");
            }
            sb.Append(")");

            return sb.ToString();
        }

        //Uses Otsu's Threshold algorithm to convert from grayscale to black and white
        private void otsu()
        {
            int[] histogram = new int[256];

            foreach (double datum in data)
            {
                histogram[(int)datum]++;
            }

            double sum = 0;
            for (int j = 0; j < histogram.Length; j++)
            {
                sum += j * histogram[j];
            }

            double sumB = 0;
            int wB = 0;
            int wF = 0;

            double maxVariance = 0;
            int threshold = 0;

            int i = 0;
            bool found = false;

            while (i < histogram.Length && !found)
            {
                wB += histogram[i];

                if (wB != 0)
                {
                    wF = data.Length - wB;

                    if (wF != 0)
                    {
                        sumB += (i * histogram[i]);

                        double mB = sumB / wB;
                        double mF = (sum - sumB) / wF;

                        double varianceBetween = wB * Math.Pow((mB - mF), 2);

                        if (varianceBetween > maxVariance)
                        {
                            maxVariance = varianceBetween;
                            threshold = i;
                        }
                    }
                    else
                    {
                        found = true;
                    }
                }

                i++;
            }

            for (i = 0; i < data.Length; i++)
            {
                data[i] = data[i] <= threshold ? 0 : 1;
            }
        }

        public int getLabel()
        {
            return label;
        }

        public double[] getData()
        {
            return data;
        }
    }
}
