﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork
{
    public class Pair<T1, T2>
    {
        public T1 Value { get; set; }
        public T2 Weight { get; set; }
        public Pair(T1 t1, T2 t2)
        {
            Value = t1;
            Weight = t2;
        }
    }
}
