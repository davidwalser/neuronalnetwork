﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    public class OutputNeuron : Neuron
    {
        public OutputNeuron()
        {
            Connections = new List<Connection>();
        }
    }
}
