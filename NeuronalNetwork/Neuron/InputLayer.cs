﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    class InputLayer : Layer
    {
        public InputLayer()
        {
        }
		
		public override void CalculateErrors()
        {
			for(int i = 0; i < Nodes.Count; i++)
            {
                Errors[i] = 0.0f;
            }
		}
    }
}
