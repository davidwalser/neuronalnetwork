﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    public abstract class Neuron
    {
        public double Value { get; set; }
        //public double Bias { get; set; } = 1;
        //public double BiasWeight { get; set; } = 0.1;
        public List<Connection> Connections { get; set; }

        /*public double BiasValuewithWeight
        {
            get
            {
                return Bias * BiasWeight;
            }
        }*/
    }
}
