﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    public class InputNeuron : Neuron
    {
        public InputNeuron()
        {
            Connections = new List<Connection>();
        }
    }
}
