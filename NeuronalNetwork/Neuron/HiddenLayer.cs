﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    class HiddenLayer : Layer
    {
        public HiddenLayer()
        {
        }
		
		public override void CalculateErrors()
        {
			for(int i = 0; i < Nodes.Count; i++)
            {
                double sum = 0;
                List<Connection> connections = Nodes[i].Connections.Where(a => a.ComingFromNeuron == Nodes[i]).ToList();
                for (int j = 0; j < ChildLayer.Nodes.Count; j++)
                {
                    sum += ChildLayer.Errors[j] * connections[j].Weight;
                }
                Errors[i] = sum * Nodes[i].Value * (1 - Nodes[i].Value);
                //Console.WriteLine("-------------------HIDDENLAYER_ERRORS---------");
                //Console.WriteLine("HIDDENNValue: " + Nodes[i].Value);
                //Console.WriteLine("HIDDENError: " + Errors[i]);
            }
		}
    }
}
