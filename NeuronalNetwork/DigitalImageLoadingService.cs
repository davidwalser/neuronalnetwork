﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NeuronalNetwork
{
    public class DigitImageLoader
    {
        private readonly string _labelFileName;
        private readonly string _imageFileName;

        public DigitImageLoader(string labelFileName, string imageFileName, int imageCount = 60000)
        {
            this._labelFileName = labelFileName;
            this._imageFileName = imageFileName;
            IMAGE_COUNT = imageCount;
        }

        /** the following constants are defined as per the values described at http://yann.lecun.com/exdb/mnist/ **/

        private int IMAGE_COUNT = 60000;

        private const int MAGIC_OFFSET = 0;
        private const int OFFSET_SIZE = 4; //in bytes

        private const int LABEL_MAGIC = 2049;
        private const int IMAGE_MAGIC = 2051;

        private const int NUMBER_ITEMS_OFFSET = 4;
        private const int ITEMS_SIZE = 4;

        private const int NUMBER_OF_ROWS_OFFSET = 8;
        private const int ROWS_SIZE = 4;
        public const int ROWS = 28;

        private const int NUMBER_OF_COLUMNS_OFFSET = 12;
        private const int COLUMNS_SIZE = 4;
        public const int COLUMNS = 28;

        private const int IMAGE_OFFSET = 16;
        private const int IMAGE_SIZE = ROWS * COLUMNS;

        public IEnumerable<DigitImage> LoadData()
        {
            DigitImage[] result = new DigitImage[IMAGE_COUNT];

            byte[] data = new byte[IMAGE_SIZE];

            var ifsPixels = new FileStream(Path.Combine("../../../Images",_imageFileName), FileMode.Open);
            var ifsLabels = new FileStream(Path.Combine("../../../Images", _labelFileName), FileMode.Open);

            var brImages = new BinaryReader(ifsPixels);
            var brLabels = new BinaryReader(ifsLabels);

            int imageMagic = brImages.ReadInt32(); // stored as big endian
            imageMagic = ReverseBytes(imageMagic); // little endian
            if (imageMagic != IMAGE_MAGIC) throw new ArgumentException();

            int imageCount = brImages.ReadInt32();
            imageCount = ReverseBytes(imageCount);
            if (imageCount != IMAGE_COUNT) throw new ArgumentException();

            int numberOfRows = brImages.ReadInt32();
            numberOfRows = ReverseBytes(numberOfRows);
            if (numberOfRows != ROWS) throw new ArgumentException();

            int numberOfColumns = brImages.ReadInt32();
            numberOfColumns = ReverseBytes(numberOfColumns);
            if (numberOfColumns != COLUMNS) throw new ArgumentException();

            int labelMagic = brLabels.ReadInt32();
            labelMagic = ReverseBytes(labelMagic);
            if (labelMagic != LABEL_MAGIC) throw new ArgumentException();

            int labelCount = brLabels.ReadInt32();
            labelCount = ReverseBytes(labelCount);
            if (labelCount != IMAGE_COUNT) throw new ArgumentException();

            for (int di = 0; di < IMAGE_COUNT; ++di)
            {
                for (int i = 0; i < IMAGE_SIZE; i++)
                {
                    byte b = brImages.ReadByte();
                    data[i] = b;
                }
                byte label = brLabels.ReadByte();
                var dImage = new DigitImage(label, data);
                result[di] = dImage;
            }
            ifsPixels.Close(); brImages.Close();
            ifsLabels.Close(); brLabels.Close();
            return result;
        }

        public static int ReverseBytes(int v)
        {
            byte[] intAsBytes = BitConverter.GetBytes(v);
            Array.Reverse(intAsBytes);
            return BitConverter.ToInt32(intAsBytes, 0);
        }

    }
}
    

