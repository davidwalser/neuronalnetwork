﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    public class Connection
    {
        public double Weight { get; set; }
        public double WeightChange { get; set; }
        public Neuron ComingFromNeuron { get; set; }
        public Neuron GoingToNeuron { get; set; }
        public double ComingNeuronValuewithWeight { get { return ComingFromNeuron.Value * Weight; } }
        public double GoingNeuronValuewithWeight { get { return GoingToNeuron.Value * Weight; } }
    }
}
