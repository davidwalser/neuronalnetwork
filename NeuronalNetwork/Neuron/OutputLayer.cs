﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    class OutputLayer : Layer
    {
        public int[] DesiredValues { get; set; } = new int[1];

        public OutputLayer()
        {
        }
		
		public override void CalculateErrors()
        {
			for(int i = 0; i < Nodes.Count; i++)
            {        
                Errors[i] = (DesiredValues[i] - Nodes[i].Value) * Nodes[i].Value * (1 - Nodes[i].Value);
                //Console.WriteLine("-------------------OUTPUTLAYER_ERRORS---------");
                //Console.WriteLine($"Desired: " + DesiredValues[i]);
                //Console.WriteLine("OUTNodeValue: " + Nodes[i].Value);
                //Console.WriteLine("OutputError: " + Errors[i]);
            }
            //Console.WriteLine("----------------------------");
        }
    }
}
