﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    public class HiddenNeuron : Neuron
    {
        public HiddenNeuron()
        {
            Connections = new List<Connection>();
        }

    }
}
