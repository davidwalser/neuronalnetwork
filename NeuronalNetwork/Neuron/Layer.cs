﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuronalNetwork.Neuron
{
    public abstract class Layer
    {
        public List<Neuron> Nodes { get; set; } = new List<Neuron>();
        public List<Pair<double,double>> Bias { get; set; } = new List<Pair<double,double>>();
        public string name;
        public double[] Errors { get; set; }
        public Layer ParentLayer { get; set; }
        public Layer ChildLayer { get; set; }
        public bool UseMomentum { get; set; } = false;
        public double LearningRate { get; set; } = 0.2;
        public double MomentumFactor { get; set; } = 0.9;
        int asdf = 0;

        public void CalculateNeuronValues()
        {
            double x = 0.0;
            if (ParentLayer != null)
            {
                for (int j = 0; j < Nodes.Count; j++)
                {
                    asdf++;
                    x = 0.0;
                    //Console.WriteLine($"-------{name} WEIGHTScomingfrom-------");
                    List<Connection> connections = Nodes[j].Connections.Where(a => a.GoingToNeuron == Nodes[j]).ToList();
                    for (int i = 0; i < ParentLayer.Nodes.Count; i++)
                    {
                        //Console.WriteLine("WEIGHT: " + connections[i].Weight);
                        x += connections[i].ComingNeuronValuewithWeight;
                        if (double.IsNaN(Nodes[j].Connections[i].ComingNeuronValuewithWeight))
                        {
                            Console.WriteLine("RIP");
                            Console.ReadKey();
                        }
                    }
                    x += ParentLayer.Bias[j].Value * ParentLayer.Bias[j].Weight;
                    //Console.WriteLine("BiaswithValue " + Nodes[j].Connections[j].ComingFromNeuron.BiasValuewithWeight);
                    if (ChildLayer == null)
                    {
                        Nodes[j].Value = 1.0 / (1.0 + Math.Exp(-x));
                    }
                    else
                    {
                        Nodes[j].Value = 1.0 / (1.0 + Math.Exp(-x));
                    }
                    //Console.WriteLine($"{name} new Value " + Nodes[j].Value);
                }
            }
        }

        public virtual void CalculateErrors()
        {
        }

        public void AdjustWeights()
        {
            double dw = 0.0;
            if (ChildLayer != null)
            {
                for (int i = 0; i < Nodes.Count; i++)
                {
                    List<Connection> connections = Nodes[i].Connections.Where(a => a.ComingFromNeuron == Nodes[i]).ToList();
                    //Console.WriteLine($"----------{name} Adjust Weights --------");
                    for (int j = 0; j < ChildLayer.Nodes.Count; j++)
                    {
                        //Console.WriteLine("CHILDERRORS: " + ChildLayer.Errors[j]);
                        dw = LearningRate * ChildLayer.Errors[j] * Nodes[i].Value;
                        //Console.WriteLine($"{name} DW: {dw}");

                        if (UseMomentum)
                        {
                            connections[j].Weight += dw + MomentumFactor * connections[j].WeightChange;
                            connections[j].WeightChange = dw;
                            //Nodes[i].Connections[j].Weight += dw + MomentumFactor * Nodes[i].Connections[j].WeightChange;
                            //Nodes[i].Connections[j].WeightChange = dw;
                        }
                        else
                        {
                            //Nodes[i].Connections[j].Weight += dw;
                            connections[j].Weight += dw;
                        }
                    }
                }
                for (int j = 0; j < ChildLayer.Nodes.Count; j++)
                {
                    Bias[j].Weight = LearningRate * ChildLayer.Errors[j] * Bias[j].Value;
                    //Nodes[j].BiasWeight += LearningRate * Errors[j] * Nodes[j].Bias;
                    //Console.WriteLine($"{name} new BiasWeight: {Nodes[j].BiasWeight}");
                }
            }
        }
    }
}
